import * as express from "express";
import "../scss/styles";

const port: number = 9001;

const app: express.Application = express();

app.set('view engine', 'ejs');
app.use(express.static('public')),


app.get( '/', ( req: express.Request, res: express.Response ) => {

    res.render('home');

});

app.listen( port, () => { console.log( `Listening on port ${port}` ); });
